
PODMAN=podman
REGISTRY=quay.io/drachlyznardh
IMAGE=$(PROG)-$(NAME)

all: build

build:
	@$(PODMAN) build -t $(REGISTRY)/$(IMAGE):$(VERSION) .
	@$(PODMAN) push $(REGISTRY)/$(IMAGE):$(VERSION)

