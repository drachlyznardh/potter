
import sys, subprocess, json, pkg_resources, os.path

if sys.version_info.major > 3 or sys.version_info.minor > 6:
    def execCommand(command, doCapture): return subprocess.run(command, capture_output=doCapture)
else:
    def execCommand(command, doCapture): return subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

class Potter(object):
    def __init__(self, verbose=0):
        with open(pkg_resources.resource_filename(__name__, 'VERSION'), 'rt', encoding='utf-8') as ifd:
            self.version = ifd.read().strip()
        self.prog = __package__
        self.verbose = verbose
        self.platform = 'podman'
        # self.registry = 'localhost'
        self.registry = 'quay.io/drachlyznardh'
        self.bake = 'bake'
        self.testPot = 'test'

    def setup(self, args):
        self.verbose = args.verbose
        self._dprint(3, '{}', args)

    def _eprint(self, messageFormat, *args, **kwargs): print(messageFormat.format(*args, **kwargs), file=sys.stderr)
    def _xprint(self, exitCode, messageFormat, *args, **kwargs):
        self._eprint(messageFormat, *args, **kwargs)
        return exitCode
    def _dprint(self, level, messageFormat, *args, **kwargs):
        if self.verbose < level: return
        print(messageFormat.format(*args, **kwargs))

    def _image(self, name): return '{}-{}'.format(self.prog, name)
    def _runCommand(self, commandFormat, errorFormat=None, doCapture=True, **kwargs):
        def helper(potter, command, doCapture=True):
            class Result:
                def __init__(self, result, running=True, mustRemove=False):
                    self.exitCode = result.returncode
                    self._bOut = result.stdout
                    self._out = None
                    self._bErr = result.stderr
                    self._err = None
                    self.isRunning = running
                    self.mustRemove = mustRemove
                def __bool__(self): return not self.exitCode
                def __int__(self): return self.exitCode
                def getOut(self):
                    if not self._out: self._out = self._bOut.decode('utf-8').strip() if self._bOut else ''
                    return self._out
                def getErr(self):
                    if not self._err: self._err = self._bErr.decode('utf-8').strip() if self._bErr else ''
                    return self._err
                def setRunning(self, running):
                    self.isRunning = running
                    return self
                def setMustRemove(self, mustRemove):
                    self.mustRemove = mustRemove
                    return self
                def override(self, value):
                    self.exitCode = value
                    return self
            potter._dprint(1, '\tNow calling $( {} )'.format(' '.join(command)))
            result = Result(execCommand(command, doCapture))
            potter._dprint(2, '\tExit code was {}', result.exitCode)
            if potter.verbose > 2:
                potter._dprint(3, 'STDOUT: {}', result.getOut())
                potter._dprint(3, 'STDERR: {}', result.getErr())
            return result

        result = helper(self, [e for e in [self.platform] + commandFormat.format(**kwargs).split(' ') if e], doCapture)
        if result: return result
        if errorFormat:
            self._eprint(result.getErr())
            self._eprint(errorFormat.format(**kwargs))
        return result

    def _doHelper(self, call, args, names):
        if type(names) is str: return call(self, args, names)
        if len(names) == 1: return call(self, args, names[0])
        return sum([call(self, args, e) for e in names])

    def do_check(self, args, names):
        def helper(self, args, name):
            result = self._runCommand('inspect {image}', image=self._image(name))
            if not result: return result.setRunning(False)
            containerState = json.loads(result.getOut())[0].get('State')
            isRunning = containerState.get('Running')
            status = containerState.get('Status')
            result.setRunning(isRunning)
            result.setMustRemove(not isRunning and status in ('configured'))
            self._dprint(2, 'State.Running is {}', result.isRunning)
            return result
        return self._doHelper(helper, args, names)

    def do_start(self, args, names):
        def helper(self, args, name):
            result = self.do_check(args, name)
            if result.isRunning: return result.override(args.hard)
            if result.mustRemove:
                if not self._runCommand('rm {image}', 'Could not remove pot {pot}, which was holding the name hostage',
                    image=self._image(name), pot=name): return 2
            return self._runCommand('run --name {image} --rm -d {registry}/{image}:{version} sleep infinity', image=self._image(name), registry=self.registry, version=self.version)
        return self._doHelper(helper, args, names)

    def do_stop(self, args, names):
        def helper(self, args, name):
            result = self.do_check(args, name)
            if not result.isRunning: return result.override(args.hard)
            return self._runCommand('stop {image}', image=self._image(name))
        return self._doHelper(helper, args, names)

    def do_login(self, args, name):
        return self._runCommand('exec -u {user} -it {image} bash', doCapture=False, user=args.user or 'root', image=self._image(name))

    def _doCheckPotAndInput(self, args, targetPot, targetFiles):
        if not self.do_check(args, targetPot).isRunning: return self._xprint(3, '{pot} is not running', pot=targetPot)
        for name in targetFiles:
           if not os.path.exists(name): return self._xprint(4, 'File {outerPath} does not exist', outerPath=name)
        return 0

    def do_bake(self, args, outerPath, build, release):

        check = self._doCheckPotAndInput(args, self.bake, [outerPath])
        if check: return check

        basename = os.path.basename(outerPath)
        corename = basename[:-7] if basename.endswith('.tar.gz') else basename[:-4] if basename.endswith('.tgz') else os.path.split(basename)[0]
        image = self._image(self.bake)
        extractDir = '/bake'
        bakeDir = os.path.join(extractDir, corename)
        distDir = os.path.join(bakeDir, 'dist')

        if not self._runCommand('exec {image} rm -rf {workDir}', 'Could not clear workdir {workDir} inside pot {pot}',
            image=image, workDir=extractDir, pot=self.bake): return 5

        if not self._runCommand('cp {outerPath} {image}:{innerPath}', 'Could not copy file {outerPath} to pot {pot}',
            outerPath=outerPath, image=image, innerPath=os.path.join(extractDir, basename), pot=self.bake): return 6

        if not self._runCommand('exec -w {workDir} {image} tar xvzf {basename}', 'Could not extract file {outerPath} into {extractDir} inside pot {pot}',
            workDir=extractDir, image=image, basename=basename, outerPath=outerPath, extractDir=extractDir, pot=self.bake): return 7

        if not self._runCommand('exec -w {workDir} {image} python3 setup.py bdist_rpm {build} --release {release}', 'Could not build RPM from {workDir} inside pot {pot}',
            workDir=bakeDir, image=image, release=release, build='--{}-only'.format(build) if build else '', pot=self.bake): return 8

        result = self._runCommand('exec {image} ls {distDir}', 'Could not locate RPM from {distDir} inside pot {pot}',
            distDir=distDir, image=image, pot=self.bake)
        if not result: return 9

        for rpmBasename in [e for e in result.getOut().split('\n') if e.endswith('.rpm') or e.endswith('.spec')]:
            if not self._runCommand('cp {image}:{innerPath} {outerPath}', 'Could not extract RPM {innerPath} from inside pot {pot}',
                image=image, innerPath=os.path.join(distDir, rpmBasename), outerPath=rpmBasename, pot=self.bake): return 10

        return 0

    def do_spec(self, args, outerSpec, release):

        check = self._doCheckPotAndInput(args, self.bake, [outerSpec])
        if check: return check

        homeDir = '/home/user'
        specDir = os.path.join(homeDir, 'rpmbuild/SPECS')
        rpmDir = os.path.join(homeDir, 'rpmbuild/RPMS')
        innerSpec = os.path.join(specDir, os.path.basename(outerSpec))
        image = self._image(self.bake)

        if not self._runCommand('cp {outerSpec} {image}:{innerSpec}', 'Could not copy file {outerSpec} to pot {pot}', outerSpec=outerSpec, image=image, innerSpec=innerSpec, pot=self.bake):
            return 5

        if not self._runCommand('exec -w {workDir} -u user {image} rpmbuild -bb {innerSpec}', doCapture=False, workDir=homeDir, image=image, innerSpec=innerSpec):
            return 6

        result = self._runCommand('exec {image} find {rpmDir} -name *.rpm', 'Could not locate RPM from {rpmDir} inside pot {pot}',
            rpmDir=rpmDir, image=image, pot=self.bake)
        if not result: return 7

        for innerRPM in result.getOut().split('\n'):
            if not self._runCommand('cp {image}:{innerRPM} {outerRPM}', 'Could not extract file {outerRPM} from inside pot {pot}',
                    outerRPM=os.path.basename(innerRPM), image=image, innerRPM=innerRPM, pot=self.bake):
                return 8

        return 0

    def do_test(self, args, inputNames):

        check = self._doCheckPotAndInput(args, self.bake, [e for e in [args.test_file] + inputNames if e])
        if check: return check

        image = self._image(self.testPot)
        rpmDir = '/rpm'
        testDir = '/test'
        testFile = os.path.basename(args.test_file) if args.test_file else None
        basenames=[os.path.basename(e) for e in inputNames]
        rpms, packageNames = [' '.join(e) for e in zip(*[(e, os.path.splitext(e)[0]) for e in [os.path.basename(e) for e in inputNames]])]
        prerequisites = [e for e in (args.prerequisites.split(',') if args.prerequisites else []) if e]
        resFiles = [e for e in [testFile] + (args.resources.split(',') if args.resources else []) if e]

        for name in inputNames:
            if not self._runCommand('cp {outerPath} {image}:{innerPath}', 'Could not copy RPM file {outerPath} inside pot {pot}',
                outerPath=name, image=image, innerPath=os.path.join(rpmDir, os.path.basename(name)), pot=self.testPot): return 5

        for resFile in resFiles:
            if not self._runCommand('cp {outerPath} {image}:{innerPath}', 'Could not copy TEST file {resFile} inside pot {pot}',
                outerPath=resFile, image=image, innerPath=os.path.join(testDir, resFile), resFile=resFile, pot=self.testPot): return 6

        isInstalled = self._runCommand('exec {image} dnf list installed {packageNames}', 'Could not verify installation state of RPM file(s) {rpms} inside pot {pot}',
            rpmDir=rpmDir, image=image, packageNames=packageNames, rpms=rpms, pot=self.testPot)

        if not self._runCommand('exec -w {rpmDir} {image} dnf {installCommand} -y {rpms}', 'Could not install RPM file(s) {rpms} inside pot {pot}',
            doCapture=False, rpmDir=rpmDir, image=image, installCommand='reinstall' if isInstalled else 'install', rpms=rpms, pot=self.testPot): return 7

        if testFile:
            if not self._runCommand('exec -u {user} -w {testDir} {image} chmod +x {testFile}', 'Could not make test file {testFile} executable inside pot {pot}',
                doCapture=False, user=args.user or 'root', testDir=testDir, image=image, testFile=testFile, rpms=rpms, pot=self.testPot): return 8

            if not self._runCommand('exec -u {user} -w {testDir} {image} ./{testFile}', 'Could not run test file {testFile} inside pot {pot}',
                doCapture=False, user=args.user or 'root', testDir=testDir, image=image, testFile=testFile, rpms=rpms, pot=self.testPot): return 9

        if prerequisites:
            if not self._runCommand('exec {image} dnf install -y {packages}', 'Could not install RPM package(s) {packages} inside pot {pot}',
                doCapture=False, image=image, packages=' '.join(prerequisites), pot=self.testPot): return 10

        if args.command:
            if not self._runCommand('exec -u {user} -w {testDir} {image} {command}', 'Could not run command $( {command} ) inside pot {pot}',
                doCapture=False, user=args.user or 'root', testDir=testDir, image=image, command=args.command, rpms=rpms, pot=self.testPot): return 11

        return 0

