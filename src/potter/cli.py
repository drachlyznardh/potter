
import sys, argparse, argcomplete

from .command import cmd_check, cmd_start, cmd_stop, cmd_restart, cmd_login, cmd_bake, cmd_spec, cmd_test
from .potter import Potter

def _getSubParser(subList, name, command, helpMessage):
    parser = subList.add_parser(name)
    parser.set_defaults(cmd=command)
    parser.add_argument('names', action='store', nargs='+', help=helpMessage)
    return parser

def _setHard(parser, isRunning):
    parser.add_argument('--hard', action='store_true', help='Fail if pot is {}running'.format('' if isRunning else 'not '))
    parser.add_argument('--soft', action='store_false', dest='hard', help='Do not fail if pot is {}running [default]'.format('' if isRunning else 'not '))
    return parser

def _userControl(parser):
    userGroup = parser.add_mutually_exclusive_group()
    userGroup.add_argument('-u', action='store_const', dest='user', const='user', help='Login as user')
    userGroup.add_argument('--root', action='store_const', dest='user', const='root', help='Login as root')
    userGroup.add_argument('--user', action='store', help='Login as specified USER')
    return parser

def getParser(potter):
    parser = argparse.ArgumentParser(prog=potter.prog)
    parser.set_defaults(cmd=lambda args: parser.print_help())
    parser.set_defaults(potter=potter)

    parser.add_argument('--version', action='version', version='{} v{}'.format(potter.prog.capitalize(), potter.version), help='Show pretty version number, human readable')
    parser.add_argument('--machine-version', action='version', version=potter.version, help='Show plain version number, machine readable')
    parser.add_argument('-v', '--verbose', action='count', default=0, help='Increase verbosity')

    subList = parser.add_subparsers()
    _getSubParser(subList, 'check', cmd_check, 'check status of named pot')
    _setHard(_getSubParser(subList, 'start', cmd_start, 'start a pot if not running'), True)
    _setHard(_getSubParser(subList, 'stop', cmd_stop, 'stop a pot if running'), False)
    _setHard(_getSubParser(subList, 'restart', cmd_restart, 'restart a pot if running, or start a pot if not'), True)
    _userControl(_getSubParser(subList, 'login', cmd_login, 'login into a running pot'))

    bake = _getSubParser(subList, 'bake', cmd_bake, 'bake RPMs from python source archives')
    bake.add_argument('-r', '--release', action='store', default=1, help='Set RPM release number')
    group = bake.add_mutually_exclusive_group()
    group.add_argument('-b', '--binary-only', action='store_const', dest='build', const='binary', help='Build binary RPM only')
    group.add_argument('-s', '--source-only', action='store_const', dest='build', const='source', help='Build source RPM only')
    group.add_argument('--spec-only', action='store_const', dest='build', const='spec', help='Build SPEC file only')

    spec = _getSubParser(subList, 'spec', cmd_spec, 'bake RPMs from SPEC files')
    spec.add_argument('-r', '--release', action='store', default=1, help='Set RPM release number')

    test = _userControl(_getSubParser(subList, 'test', cmd_test, 'test RPMs'))
    test.add_argument('-f', '--test-file', action='store', help='Run test file after installation')
    test.add_argument('-c', '--command', action='store', help='Run command installation')
    test.add_argument('-p', '--prerequisites', action='store', help='Install RPM packages before installation')
    test.add_argument('-r', '--resources', action='store', help='Copy additional test resources')

    argcomplete.autocomplete(parser)
    return parser

def main(inArgs=sys.argv[1:]):
    args = getParser(Potter()).parse_args(inArgs)
    args.potter.setup(args)
    if args.cmd: return args.cmd(args)

