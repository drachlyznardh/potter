
import subprocess, json

def _printStatus(name, isRunning): print('Pot[{}] is {}running'.format(name, '' if isRunning else 'not '))
def _sum(call, args, names): return sum([call(args, e) for e in list({s:0 for s in names})])

def cmd_check(args):
    def helper(args, name):
        result = args.potter.do_check(args, name)
        _printStatus(name, result.isRunning)
        return result.exitCode
    return _sum(helper, args, args.names)

def cmd_start(args):
    def helper(args, name):
        result = args.potter.do_start(args, name)
        if not result: _printStatus(name, result.isRunning)
        return result.exitCode
    return _sum(helper, args, args.names)

def cmd_stop(args):
    def helper(args, name):
        result = args.potter.do_stop(args, name)
        if result: _printStatus(name, result.isRunning)
        return result.exitCode
    return _sum(helper, args, args.names)

def cmd_restart(args):
    def helper(args, name):
        result = args.potter.do_stop(args, name)
        if not result: return result.exitCode
        result = args.potter.do_start(args, name)
        cmd_check(args)
        return result.exitCode
    return _sum(helper, args, args.names)

def cmd_login(args):
    def helper(args, name):
        if args.potter.do_check(args, name): return args.potter.do_login(args, name).exitCode
        _printStatus(name, False)
        return 1
    return _sum(helper, args, args.names)

def cmd_bake(args):
    def helper(args, name): return args.potter.do_bake(args, name, args.build, args.release)
    return _sum(helper, args, args.names)

def cmd_spec(args):
    def helper(args, name): return args.potter.do_spec(args, name, args.release)
    return _sum(helper, args, args.names)

def cmd_test(args): return args.potter.do_test(args, args.names)

