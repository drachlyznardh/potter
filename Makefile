
NAME=potter
VERSION=1.0.0

SETUP=python3 setup.py
PIP=pip3

SRCDIR=src/
OUTDIR=outsrc/
OUTSRCDIR=$(OUTDIR)$(NAME)/
RESDIR=res/

PYSRCS=$(shell find $(SRCDIR) -name \*.py -type f)
OUTPYSRCS+=$(patsubst $(SRCDIR)%.py, $(OUTDIR)%.py, $(PYSRCS))
OUTSRCS+=$(addprefix $(OUTSRCDIR), VERSION)
OUTSRCS+=$(OUTPYSRCS)

POTTER_TGZ=dist/$(NAME)-$(VERSION).tar.gz
POTTER_RPM=$(NAME)-$(VERSION)-1.noarch.rpm
POTTER_DEPS_RPM=$(NAME)-deps-$(VERSION)-1.noarch.rpm
POTTER_DEPS_SPEC=$(RESDIR)$(NAME)-deps.spec
POTTER_TEST=$(RESDIR)$(NAME).test
RPMS=$(POTTER_RPM) $(POTTER_DEPS_RPM)

all: localinstall quicktest

pots:
	$(MAKE) -C pots NAME=$(NAME) VERSION=$(VERSION)

setup.py $(POTTER_DEPS_SPEC) $(POTTER_TEST): %: %.in Makefile
	@sed -e 's;@NAME@;$(NAME);g; s;@VERSION@;$(VERSION);g; s;@OUTDIR@;$(OUTDIR);g;' < $< | sed -e 's;{AT};@;g;' > $@

$(OUTDIR)%.py: $(SRCDIR)%.py | $(OUTSRCDIR)
	@cp $< $@

$(OUTSRCDIR)VERSION: Makefile | $(OUTSRCDIR)
	@echo $(VERSION) > $@

$(OUTSRCDIR):
	@mkdir -p $@

$(POTTER_TGZ): setup.py $(OUTSRCS)
	@$(SETUP) sdist

$(POTTER_RPM): $(POTTER_TGZ)
	@$(NAME) restart bake
	@$(NAME) -v bake -b $<

$(POTTER_DEPS_RPM): $(POTTER_DEPS_SPEC)
	@$(NAME) -vv spec $^

localinstall: setup.py $(OUTSRCS)
	@$(PIP) install --user .

localremove: setup.py
	@$(PIP) uninstall -y $(NAME)

sdist: $(POTTER_TGZ)
self: $(POTTER_RPM)
deps: $(POTTER_DEPS_RPM)
selftest: $(POTTER_TEST) $(RPMS)
	@$(NAME) start test
	@$(NAME) -vv test -uf $(POTTER_TEST) $(RPMS)

quicktest:
	@$(NAME)
	@$(NAME) --version
	@$(NAME) --machine-version
	@$(NAME) check doc || true

statustest:
	@$(NAME) check test || true
	@$(NAME) start test
	@$(NAME) check test
	@$(NAME) restart test
	@$(NAME) check test
	@$(NAME) stop test
	@$(NAME) check test; test $$? -ne 0

clean: setup.py
	@$(SETUP) clean

veryclean: clean
	@rm -rf setup.py dist/ $(OUTDIR) *.rpm *.spec

.PHONY: all pots localinstall localremove sdist quicktest clean veryclean

